chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab){
    // Trigger a message when change in url
    if(changeInfo && changeInfo.status == "complete"){
        chrome.tabs.sendMessage(tabId, {data: tab}, function(response) {});
    }
});