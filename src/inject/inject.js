chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
    	if (document.readyState === "complete") {
    		clearInterval(readyStateCheckInterval);

    		console.log("Youtube-To-Txt: Log - READY STATE");

            getToken(function(userid) { updateTitle(userid); });
    	}
	}, 10);
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    setTimeout(function() {
        getToken(function(userid) { updateTitle(userid); });
    }, 5000);
});

function getRandomToken() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function getToken(callback) {
    console.log("Youtube-To-Txt: Log - GET TOKEN");
    chrome.storage.sync.get('userid', function(items) {
        var userid = items.userid;
        if (userid) {
            useToken(userid);
        } else {
            userid = getRandomToken();
            chrome.storage.sync.set({userid: userid}, function() {
                useToken(userid);
            });
        }
        function useToken(userid) {
            callback(userid);
        }
    });
}

function updateTitle(userid) {
    console.log("Youtube-To-Txt: Log - CURRENT VIDEO - " + document.title);
    console.log("Youtube-To-Txt: Log - WRITTING TO SERVER");
    var title = document.title.replace(' - YouTube', '');

    $.ajax('https://api.roguyt.ovh/video/update', {
        method: 'POST',
        data: {
            userid: userid,
            title: title
        },
        success: function(response, status) {
            console.log("Youtube-To-Txt: Log - UPDATED");
            console.log("Youtube-To-Txt: Log - https://api.roguyt.ovh/video/" + userid);
        }
    });
}